<html>

<head>
    <link type="text/css"
          rel="stylesheet"
          href="./style.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/5.7.0/d3.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js"></script>
    <script src="https://unpkg.com/marked@0.3.6"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script defer
            src="./app.js"></script>

    <script>

    </script>
</head>

<body>
    <div id="christmas-card">
        <div v-if="!isDone"
             class="cc-wrapper">
            <div class="cc-header">
                <div class="cc-header-title">{{title}}</div>
                <div class="cc-header-status">({{100*((index) / numFrames)}} %) - ({{(index+1)}} / {{numFrames}}), {{tries}} tries, ({{totalTries}} total)</div>
                <div class="cc-header-description">{{description}}</div>
            </div>
            <div v-on:click="click($event)"
                 class="cc-content">
                <svg ref="svg"></svg>
                <img ref="edited"
                     :src="edited">
                <img ref="unedited"
                     :src="unedited">
            </div>
        </div>
        <div class="cc-scores"
             v-if="isDone">
            <scoreboard :stats="stats.byName"></scoreboard>
            <!-- <div>Score vis!</div> -->
            <!-- <vis :stats="stats.all"></vis> -->
        </div>
    </div>

    <!-- <div class="gdpr-bullshit">
        <p>GDPR Compliance: After playing the game, you will be asked for your name and group.
            You are free to use any name/group you want to.
            Your name will be stored and displayed on the high score list (if you do well!).</p>
    </div> -->

    <div ref="container"
         id="snow">
        <canvas ref="canvas"></canvas>
    </div>
</body>

</html>
