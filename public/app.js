const PARAMS = {
    showtime: 3000,
    hidetime: 700,
    svgcolor: 'white',
    nextLevelDelay: 4500,
    nextLevelShowtime: 500
};

window.xmas = PARAMS;

const HIT_RECT = (checkX, checkY, {
        x,
        y,
        width,
        height
    }) =>
    x <= checkX && checkX <= (x + width) &&
    y <= checkY && checkY <= (y + height);

class Game {
    constructor({
        pics,
        order = []
    }) {
        if (order.length === 0) order = _.keys(pics);

        this._startTime = null;
        this._time = null; // time obj

        this._totalTries = 0;
        this._stats = [{
            tries: -1
        }]; // [{tries, time}]

        this._pics = pics;
        this._order = order;
        this._index = 0;

        this._on = {};
    }

    get tries() {
        return this._stats[this.index].tries;
    }
    get timeElapsed() {
        return Date.now() - this._time;
    }
    get totalTimeElapsed() {
        return Date.now() - this._startTime;
    }
    get numFrames() {
        return this._order.length;
    }
    get index() {
        return this._index;
    }

    on(evt, callback) {
        if (_.isFunction(callback)) this._on[evt] = callback;
        else this._on[evt] && this._on[evt].call(this);
    }

    click(naturalX, naturalY) {
        const {
            bounds
        } = this.activeFrame.difference;
        this._incrementLevelTries();

        if (HIT_RECT(naturalX, naturalY, bounds)) {
            this._stopLevelTime();
            this._index++;

            // if (this.index === this.numFrames)
            //     this.on('done');
            this.checkDone();

            this.startLevel();
            return true;
        }
    }

    skipAll() {
        this._stopLevelTime();
        this._index = this.numFrames;
        this.on('done');
    }

    skip() {
        this._stopLevelTime();
        this._index++;
        this.checkDone();

        this.startLevel();
    }

    checkDone() {
        if (this.index === this.numFrames)
            this.on('done');
    }

    get naturalDiffBounds() {
        return this.activeFrame.difference.bounds;
    }

    _incrementLevelTries() {
        this._stats[this.index].tries++;
        this._totalTries++;
    }
    _stopLevelTime() {
        this._stats[this.index].time = Date.now() - this._time;
    }

    get(index) {
        return this._pics[this.order[index]];
    }

    get activeFrame() {
        return this.get(this.index);
    }

    startLevel(index = this._index) {
        this._index = index;
        const frame = this.activeFrame;
        this._time = Date.now();
        this._stats[this.index] = {
            tries: 0
        };
        return frame;
    }

    restart() {
        return this.start();
    }
    start() {
        this._totalTries = 0;
        this.startLevel(0);
        this._startTime = Date.now();
    }

    get order() {
        return this._order;
    }

}

const pics = {};
const order = [];

const pic = (name, edited, unedited, bounds, title, background) => {
    order.push(name);
    pics[name] = {
        background,
        edited,
        unedited,
        difference: {
            bounds: {
                x: bounds[0],
                y: bounds[1],
                width: bounds[2],
                height: bounds[3]
            }
        },
        title
    };
};

pic('firefly', './pics/Firefly_modified.png', './pics/Firefly_origin.png', [382, 434, 125, 89], 'Firefly: Illumination Drones for Interactive Visualization', '#2e3640');
pic('molecules1', './pics/molV2A.png', './pics/molV2B.png', [825, 870, 161, 164], 'Protein-Protein Interactions from COZOID');
pic('vdscm', './pics/HeartNew2modified.png', './pics/HeartNew2orig.png', [539, 492, 171, 81], 'Visual Data Science & Computational Medicine', '#eeedee');
pic('vis', './pics/visA.jpg', './pics/visB.jpg', [779, 621, 108, 55], 'Visual Analysis of Ligand Trajectories', '#f3f7fa');

// pic('groupPic1', './pics/groupPic1Unedited.jpg', './pics/groupPic1TanishHelwig.jpg', [564, 314, 49, 46], 'Group Photo 1', '#fec45f');
// pic('groupPic2', './pics/groupPic1Unedited.jpg', './pics/groupPic1TanHelwig.jpg', [564, 314, 49, 46], 'Group Photo 1', '#fec45f');
// pic('groupPic3', './pics/groupPic1Unedited.jpg', './pics/groupPic1VeronicaHat.jpg', [788, 274, 60, 45], 'Group Photo 1', '#fec45f');
pic('groupPic4', './pics/groupPic1Unedited.jpg', './pics/groupPic1WatermelonChaoran.jpg', [432, 232, 48, 32], 'Group Photo 1', '#fec45f');

pic('groupRegularSergejGrey', './pics/groupPicRegularUnedited2.jpg', './pics/groupPicRegularHelwigSergejsHairGreyish.jpg', [564, 394, 75, 96], 'Group Photo 2', '#fec45f');
// pic('groupRegularSergej', './pics/groupPicRegularUnedited2.jpg', './pics/groupPicRegularHelwigSegejsHair.jpg', [564, 394, 75, 96], 'Group Photo 2', '#fec45f');
// pic('groupRegularHelwigTheGray', './pics/groupPicRegularUnedited2.jpg', './pics/groupPicRegularHelwigLongHair.jpg', [564, 394, 75, 96], 'Group Photo 2', '#fec45f');

pic('NoeskasBrain', './pics/brain1.png', './pics/brain2.png', [399, 156, 59, 57], "Noeska's Brain");

// pic('FabianJacket', './pics/fabianUnedited.jpg', './pics/fabianDarkerJacket.jpg', [0, 470, 428, 349], 'Fabian', '#c0b490');
pic('FabianDarkThermos', './pics/fabianUnedited.jpg', './pics/fabianDarkerThermos.jpg', [1080, 414, 91, 202], 'Fabian', '#c0b490');
// pic('FabianScreen', './pics/fabianUnedited.jpg', './pics/fabianScreenChg.jpg', [676, 176, 146, 199], 'Fabian', '#c0b490');

pic('Juraj', './pics/juraj_3c.jpg', './pics/juraj_3d.jpg', [39, 1017, 222, 214], 'Juraj', '#ede9e0');

pic('Helwig', './pics/santaHelwig.jpg', './pics/santaHelwigDeepBeard.jpg', [726, 91, 226, 172], 'Helwig', '#526144');

pic('Stefan', './pics/wizardStefanFlip.jpg', './pics/wizardStefanNoFlip.jpg', [336, 50, 81, 204], 'Stefan', '#ebddd0');

pic('Yngve', './pics/yngveEdited.jpg', './pics/yngveUnedited.jpg', [312, 394, 215, 212], 'Yngve', '#916c4e');

const theGame = new Game({
    pics,
    order,
});

// --------------------------------------------------------------------------
// ------------------------------+ ANIMATION +-------------------------------
// --------------------------------------------------------------------------
// const SHOWTIME = PARAMS.showtime,
//     HIDETIME = PARAMS.hidetime;
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

let __ID = 0;

class AnimConfig {
    constructor({ svg, animation }) {
        this._animation = animation;
        this._svg = svg;
        this._kill = false;
        this._fill = null;
        this._showtime = PARAMS.showtime;
        this._id = __ID++;
    }

    kill() { this._kill = true; }
    get killed() { return this._kill; }

    get showtime() { return this._showtime; }

    set fill(f) {
        this._fill = f;
        this._svg.style.background = f;
        this._showtime = PARAMS.nextLevelShowtime;
        this._animation.cycleNextLevel();
    }

    get fill() { return this._fill; }
}


function animate(index, {
    svg,
    edited,
    unedited,
}) {

    const CONFIG = new AnimConfig({ svg, animation: this });
    const bgcolor = theGame.activeFrame.background || PARAMS.svgcolor;

    const mySleep = async (ms) => {
        // if (index !== gameView.index) return false;
        if (CONFIG.killed) return false;
        await sleep(ms);
        return true;
    }

    edited.style.zIndex = 1;
    unedited.style.zIndex = 0;

    const cycle = async () => {
        console.log(`cycle: ${CONFIG._id}`)
        // Show unedited
        edited.style.zIndex = 1;
        unedited.style.zIndex = 0;
        if (!CONFIG.fill) svg.style.background = 'transparent';
        if (!await mySleep(PARAMS.showtime)) return false;
        // Show svg
        if (!CONFIG.fill) svg.style.background = bgcolor;
        if (!await mySleep(PARAMS.hidetime)) return false;

        // Show edited
        edited.style.zIndex = 0;
        unedited.style.zIndex = 1;
        if (!CONFIG.fill) svg.style.background = 'transparent';
        if (!await mySleep(PARAMS.showtime)) return false;

        // Show svg
        if (!CONFIG.fill) svg.style.background = bgcolor;
        if (!await mySleep(PARAMS.hidetime)) return false;

        // return index === gameView.index;
        return true;
    }

    const cycleNextLevel = async () => {
        console.log(`cycle: ${CONFIG._id}`)
        // Show unedited
        edited.style.zIndex = 0;
        unedited.style.zIndex = 1;
        if (!await mySleep(PARAMS.nextLevelShowtime)) return false;
        if (CONFIG.killed) return;

        // Show edited
        edited.style.zIndex = 1;
        unedited.style.zIndex = 0;
        if (!await mySleep(PARAMS.nextLevelShowtime)) return false;

        if (!CONFIG.killed) cycleNextLevel();
    };

    this.cycleNextLevel = cycleNextLevel;

    const execAnimation = async () => {
        const keepGoing = await cycle();
        if (keepGoing && !CONFIG.killed) execAnimation();
        else {
            edited.style.zIndex = 1;
            unedited.style.zIndex = 0;
            svg.style.background = bgcolor;
        }
    }

    execAnimation();

    return CONFIG;
}

// --------------------------------------------------------------------------


// --------------------------------------------------------------------------
// -------------------------------+ DATABASE +-------------------------------
// --------------------------------------------------------------------------

const SEND = async (name = 'Anonymous', group = 'Unknown', stats) =>
    await axios.get(`/game?stats=${JSON.stringify(stats)}&name=${name}&group=${group}`);

const GET_DB = () =>
    axios.get('/data');
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// --------------------+ VUE SCOREBOARD COMPONENT +--------------------------
// --------------------------------------------------------------------------
Vue.component('scoreboard', {
    data: () => ({
        data: false
    }),
    props: {
        'stats': Array,
        'myTime': Number
    },
    created: function () {
        console.log('SCOREBOARD is created here!');
    },
    updated: function () {
        console.log('SCOREBOARD Updated!');
        this.$nextTick(function () {


            // Start animation, init listeners etc!
        })
    },
    watch: {
        'stats': function (newValue, oldValue) {
            // this.data = newValue;
        }
    },
    template: `
    <div class="cc-scoreboard-container" v-if="stats.length > 0">
    <table>
        <tr>
            <th class="cc-scoreboard-row-name-header">#</th>
            <th class="cc-scoreboard-row-name-header">Name</th>
            <th class="cc-scoreboard-row-group-header">Group</th>
            <th class="cc-scoreboard-row-best-header">Best time</th>
            <th class="cc-scoreboard-row-gameplays-header">Gameplays</th>
        </tr>
        <tr class="cc-scoreboard-row" v-for="(arr, i) in stats">
            <td class="cc-scoreboard-row-name">{{i + 1}}</td>
            <td class="cc-scoreboard-row-name">{{arr[0]}}</td>
            <td class="cc-scoreboard-row-group">{{arr[1].group}}</td>
            <td class="cc-scoreboard-row-best">{{arr[1].best}}</td>
            <td class="cc-scoreboard-row-gameplays">{{arr[1].gameplays}}</td>
        </tr>
    </table>
    </div>
    `,

})
Vue.component('vis', {
    data: () => ({
        data: false
    }),
    props: {
        'stats': Array,
        'myTime': Number
    },
    created: function () {
        console.log('VIS is created here!');
        this.$nextTick(function () {
            const {
                svg,
                container
            } = this.$refs;

            const {
                width,
                height
            } = container.getBoundingClientRect();
            const svgSelection = d3.select(svg)
                .attr('width', width)
                .attr('height', height);

            const data = this.stats;

            const g = svgSelection.append('g');

            const domainTimestamp = d3.extent(data, (d) => d.Time),
                domainTime = d3.extent(data, (d) => d.sumTime);

            const scaleX = d3.scaleTime()
                .domain(domainTimestamp)
                .range([0, width]),

                scaleY = d3.scaleLog()
                .domain([180, 0]) // ms -> s
                .range([height, 0]);

            const circles =
                g
                .selectAll('circle')
                .data(data)
                .enter()
                .append('circle')
                .attr('cx', (d) => scaleX(d.Time))
                .attr('cy', (d) => scaleY(d.sumTime / 1000))
                .attr('r', 10)
                .attr('class', 'cc-circle');

            const axisBottomG = g.append('g');

            const hpad = 20;
            g.append('g')
                .call(
                    d3.axisBottom()
                    .scale(scaleX)
                    .ticks(4)
                )
                .attr('transform', `translate(0,${height + hpad})`)

            const wpad = 20;
            g.append('g')
                .call(
                    d3.axisLeft()
                    .scale(scaleY)
                    // .ticks(d3.timeSecond(2))
                    .tickSize(-(wpad + width))
                )
                .attr('transform', `translate(${-wpad},0)`)



        })
    },
    updated: function () {
        console.log('VIS Updated!');
        this.$nextTick(function () {
            const {
                svg,
                container
            } = this.$refs;
            console.log("ddd");
            // Start animation, init listeners etc!
        })
    },
    template: `
    <div ref="container" class="cc-vis-container" v-if="stats.length > 0">
        <svg ref="svg"></svg>
    </div>
    `,
})

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// ----------------------------+ VUE UI LOGIC +------------------------------
// --------------------------------------------------------------------------

const SNOWFLAKES = ({
    timeoutJitter,
    maxRate,
    width,
    height,
    radiusMin = 4,
    radiusMax = 10,
    winds = [0.2, 0.1, -0.2, -0.3, 0.2, 0.1, 0.4, 0.1],
    jitterWind = 1,
    windPersistence = 35,
    speedY = 1.1
}) => {
    const {
        flakes,
        interval
    } = maxRate;

    let index = 0;
    const activeFlakes = {};

    const windLevels = winds.length - 1;

    const windsTimes = {};
    const windsCache = {};
    const lookupWind = (i, y) => {
        if (windsCache[i] == null || windsTimes[i]++ === windPersistence) {
            const y01 = y / height;
            const windLevelIndex = Math.round(y01 * windLevels);
            const wind = /* winds[windLevelIndex] +  */ (Math.random() > 0.5 ? -1 : 1) * Math.random() * jitterWind;

            windsCache[i] = windsCache[i] != null ? (windsCache[i] + wind) / 2 : wind;
            windsTimes[i] = 0;
        }

        return windsCache[i];
    };

    const randomRadius = () => Math.random() * (radiusMax - radiusMin) + radiusMin;
    const spawnFlake = () => {
        const x = Math.random() * width,
            y = 0;

        ++index;
        windsTimes[index] = 0;
        activeFlakes[index] = {
            x,
            y,
            r: randomRadius(),
            i: index
        };
    }

    let onStepCallback = () => {};

    const stepFlakes = () => {
        const removeIndices = {};
        _.forOwn(activeFlakes, (flake, i) => {
            flake.y += speedY;

            if (flake.y >= height) {
                removeIndices[flake.i] = true;
                return;
            }

            const myWind = lookupWind(flake.i, flake.y);

            flake.x += myWind;
        });

        _.forOwn(removeIndices, (v, i) => {
            delete activeFlakes[i]
            delete windsTimes[i]
            delete windsCache[i]
        });

        onStepCallback(activeFlakes);
    }

    const spawnFlakes = () => {
        const nFlakes = flakes;

        _.times(nFlakes, (i) => {
            const i01 = (i / (nFlakes - 1));

            let time;
            if (i01 === 0 || i01 === 1)
                time = Math.random() * timeoutJitter + i01 * interval;
            else
                time = (Math.random() > 0.5 ? -1 : 1) * Math.random() * timeoutJitter + i01 * interval;

            setTimeout(spawnFlake, time);
        })
    }

    const spawnTimer = setInterval(spawnFlakes, interval);
    const stepTimer = setInterval(stepFlakes, 16.67);

    return {
        activeFlakes,
        stepTimer,
        spawnTimer,
        destroy: () => {
            clearTimeout(stepTimer);
            clearTimeout(spawnTimer);
        },
        onStep: (callback) => onStepCallback = callback
    }
}

const snow = new Vue({
    el: '#snow',
    data: {
        snowRunner: false
    },
    created: function () {
        console.log('View is created here!');
        this.$nextTick(function () {
            const {
                container,
                canvas
            } = this.$refs;

            const {
                width,
                height
            } = container.getBoundingClientRect();
            // Start animation, init listeners etc!
            if (!canvas) return;

            canvas.width = width;
            canvas.height = height;

            const ctx = canvas.getContext('2d');

            // const canvasSelection = d3.select(canvas)
            //     .attr('width', width)
            //     .attr('height', height);

            const {
                onSpawn,
                onStep
            } = SNOWFLAKES({
                timeoutJitter: 100,
                maxRate: {
                    flakes: 4,
                    interval: 1000,
                },
                width,
                height
            });

            // onSpawn((newFlakes) => {

            // });

            const LineDash = [1.6],
                StrokeWidth = 6;

            const TwoPI = Math.PI * 2;
            const render = (flakes) => {
                ctx.clearRect(0, 0, width, height);

                ctx.fillStyle = "rgba(255,255,255,0.8)";
                ctx.beginPath();

                ctx.setLineDash(LineDash);
                ctx.lineWidth = StrokeWidth;
                ctx.strokeStyle = ctx.fillStyle;

                _.forOwn(flakes, (flake) => {
                    ctx.moveTo(flake.x, flake.y);
                    ctx.arc(flake.x, flake.y, flake.r, 0, TwoPI, true)
                });

                ctx.fill();
                // ctx.stroke();

            }

            onStep(render);
        })
    },
})

const gameView = new Vue({
    el: '#christmas-card',
    data: {
        index: 0,
        tries: 0,
        totalTries: 0,
        phase: 'load',
        isDone: false,
        stats: []
        // frame: frames[0],
    },
    created: function () {
        console.log('View is created here!');
    },
    updated: function () {
        console.log('Updated!');
        // this.$nextTick(function () {
        //     this.resize();

        //     // Start animation, init listeners etc!
        // })
    },
    computed: {
        edited: {
            get: (vm) => vm.frame().edited
        },
        unedited: {
            get: (vm) => vm.frame().unedited
        },
        difference: {
            get: (vm) => vm.frame().difference
        },
        numFrames: {
            get: () => theGame.numFrames
        },
        title: {
            get: (vm) => vm.frame().title || frames[vm.index]
        },
        description: {
            get: (vm) => vm.frame().description || vm.frame().title || frames[vm.index]
        },
    },
    watch: {
        tries: function () {
            return theGame.tries;
        },
        totalTries: function () {
            return theGame.totalTries;
        },
    },
    methods: {
        timeElapsed: function () {
            return theGame.timeElapsed;
        },
        totalTimeElapsed: function () {
            return theGame.totalTimeElapsed;
        },
        frame: function () {
            return theGame.get(this.index) || {};
        },
        next: function () {
            let index = this.index + 1;
            if (index === frames.length) index = 0;
            this.index = index;
        },
        resize: function () {
            const {
                svg,
                edited
            } = this.$refs;
            if (!edited) return;

            const {
                width,
                height
            } = edited.getBoundingClientRect();

            svg.setAttribute('width', width);
            svg.setAttribute('height', height);
        },
        click: function (event) {
            const {
                svg,
                edited
            } = this.$refs;
            const {
                naturalWidth,
                naturalHeight
            } = edited;

            const {
                x,
                y,
                width,
                height
            } = svg.getBoundingClientRect();
            const {
                clientX,
                clientY
            } = event;

            const x01 = (clientX - x) / width,
                y01 = (clientY - y) / height;

            const xNatural = x01 * naturalWidth,
                yNatural = y01 * naturalHeight;


            if ((0 <= xNatural && xNatural <= naturalWidth) &&
                (0 <= yNatural && yNatural <= naturalHeight)) {

                console.log(`(${x01}, ${y01}), (${xNatural}, ${yNatural})`);

                this.tries = theGame.tries + 1;
                this.totalTries = theGame._totalTries + 1;
                const { naturalDiffBounds } = theGame;

                if (theGame.click(xNatural, yNatural)) {
                    playPositive();

                    this.animation.fill = 'transparent';

                    const w01 = naturalDiffBounds.width / naturalWidth,
                        h01 = naturalDiffBounds.height / naturalHeight,
                        x01 = naturalDiffBounds.x / naturalWidth,
                        y01 = naturalDiffBounds.y / naturalHeight;

                    const w = w01 * width,
                        h = h01 * height,
                        x = x01 * width,
                        y = y01 * height;

                    const d3svg = d3.select(svg);

                    d3svg
                        .append('rect')
                        .attr('width', w)
                        .attr('height', h)
                        .attr('x', x)
                        .attr('y', y)
                        .attr('fill-opacity', 0)
                        .attr('stroke-width', 4)
                        .attr('stroke-linejoin', 'round')
                        .attr('stroke', 'red')
                        .attr('fill', 'black');

                    // svg.style.display = 'none';
                    setTimeout(() => {
                        d3svg.selectAll('rect').remove();
                        this.index++;

                        this.tries = 0;
                        // svg.style.display = 'block';
                        this.startLevel();
                    }, PARAMS.nextLevelDelay)
                } else
                    playNegative();
            }
        },
        skip: function (event) {
            // playNegative();
            this.index++;
            theGame.skip();
            this.cheat = true;

            this.startLevel();
        },
        skipAll: function (event) {
            // playNegative();
            this.index = theGame.numFrames;
            theGame.skipAll();
            this.cheat = true;

            this.startLevel();
        },
        finish: async function (stats) {
            // Post stats to server
            this.phase = 'done';

            // TMP, disable scoreboard until server bullshit is sorted out
            if (1) return;

            const name = prompt('Name', 'Anonymous'),
                group = prompt('Your', 'N/A');

            const {
                data
            } = await SEND(name, group, stats);

            const parsedData = data.data.map((v) => {
                const parsedStats = JSON.parse(v.Stats);
                const sumTime = d3.sum(parsedStats, ({
                        time
                    }) => time),
                    sumTries = d3.sum(parsedStats, ({
                        tries
                    }) => tries);

                return Object.freeze(_.assign({}, v, {
                    Stats: parsedStats,
                    sumTime,
                    sumTries
                }));
            });

            const byName =
                _.chain(parsedData)
                .groupBy((v) => v.Name.toLowerCase())
                .mapValues((vals) => {
                    const best = d3.min(vals, ({
                            sumTime
                        }) => sumTime),
                        gameplays = vals.length;
                    return {
                        best,
                        gameplays,
                        group: vals[0].Group
                    };
                })
                .toPairs()
                .sortBy((v) => v[1].best)
                .value();

            this.isDone = true;
            this.stats = Object.freeze({
                byName,
                all: parsedData
            });
            /* Object.freeze({
                               byName,
                               data
                           }); */

            // Then: Render stats!
            return data.data;

            // const data = GET_DB();
        },
        play: function () {
            this.phase = 'play';

            this.$nextTick(() => {
                const {
                    edited,
                    unedited,
                    svg
                } = this.$refs;

                edited.style.zIndex = 1;
                unedited.style.zIndex = 0;
                svg.style.background = 'transparent';

                let editedLoaded = false,
                    uneditedLoaded = false,
                    gameStarted = false;
                const resizeSVG = () => {
                    if (!editedLoaded || !uneditedLoaded) return;

                    edited.style.zIndex = 1;
                    unedited.style.zIndex = 0;
                    svg.style.background = 'transparent';

                    this.resize();
                    this.startLevel();
                    editedLoaded = uneditedLoaded = false;
                }

                edited.onload = () => {
                    editedLoaded = true;
                    resizeSVG();
                }
                unedited.onload = () => {
                    uneditedLoaded = true;
                    resizeSVG();
                }

                // this.resize();
                theGame.start();
                this.startLevel();
            });
        },
        startLevel: function () {
            // const { svg, edited, unedited } = this.$refs;

            if (this.animation) this.animation.kill();
            this.animation = animate(this.index, this.$refs);
            console.log('start!');
        }
    }
});
// --------------------------------------------------------------------------

const loadImage = async (path) => new Promise((resolve, reject) => {
    const img = new Image();
    img.onload = () => resolve();
    img.src = path;
});

const loadImages = async (images) => {
    if (images.length === 0) return true;

    const img = images.shift();
    await loadImage(img);
    loadImages(images);
}

// List generated from running the paths-scan.sh in the sound folder
// If adding a batch of audio files, simply run
// sh paths-scan.sh and COPY PASTE these two lists in again.
const ALL_AUDIOS_NEGATIVE = [
    "./sound/negative/Fabian_negative.mp3",
    "./sound/negative/Fabian_negative2.mp3",
    "./sound/negative/Sergej_negative.mp3",
    "./sound/negative/chaoran-negative.mp3",
    "./sound/negative/fourough-no.mp3",
    "./sound/negative/fourough-no2.mp3",
    "./sound/negative/helwig-hieraurauf.mp3",
    "./sound/negative/helwig-naah.mp3",
    "./sound/negative/helwig-nono.mp3",
    "./sound/negative/helwig-notwirklich.mp3",
    "./sound/negative/helwig-oberneeht.mp3",
    "./sound/negative/neee (nooo).wav",
    "./sound/negative/neineinei.mp3",
    "./sound/negative/nie nie (no no).wav",
    "./sound/negative/no.wav",
    "./sound/negative/noeska-neij.mp3",
    "./sound/negative/noeska-niethroot.mp3",
    "./sound/negative/spatne (wrong).wav",
    "./sound/negative/tam nie je (not there).wav",
    ];

const ALL_AUDIOS_POSITIVE = [
    "./sound/positive/Fabian_positive.mp3",
    "./sound/positive/Fabian_really_positive.mp3",
    "./sound/positive/Sergej_short.mp3",
    "./sound/positive/chaoran-positive.mp3",
    "./sound/positive/dobre (correct).wav",
    "./sound/positive/flink.mp3",
    "./sound/positive/fourough-yes.mp3",
    "./sound/positive/fourough-yes2.mp3",
    "./sound/positive/helwig-bravo2.mp3",
    "./sound/positive/helwig-bravoclap.mp3",
    "./sound/positive/helwig-lovely-office.mp3",
    "./sound/positive/jajaja.mp3",
    "./sound/positive/juuuu.wav",
    "./sound/positive/mhmmmm(yes).wav",
    "./sound/positive/noeska-hroot.mp3",
    "./sound/positive/noeska-jatoch.mp3",
    "./sound/positive/spravne(correct).wav",
    "./sound/positive/veldigbra.mp3",
    "./sound/positive/wheeee (reaction to accepted paper).wav",
    "./sound/positive/yes.wav",
    ];

const audioPositive = [],
    audioNegative = [];

const picki = (arr) => Math.round(Math.random() * (arr.length - 1));
const playPositive = () => audioPositive[picki(audioPositive)].play(),
    playNegative = () => audioNegative[picki(audioNegative)].play();

const LOAD_TIME_MIN = 2500;

const BOOT = async () => {
    const allImages = [];
    order.forEach((p) => {
        const {
            edited,
            unedited
        } = pics[p];

        allImages.push(edited, unedited);
    });

    let time = Date.now();
    await loadImages(allImages);

    ALL_AUDIOS_POSITIVE.forEach((path) => audioPositive.push(new Howl({ src: [path] })));
    ALL_AUDIOS_NEGATIVE.forEach((path) => audioNegative.push(new Howl({ src: [path] })));

    const elapsedTime = Date.now() - time;
    setTimeout(() =>
        gameView.phase = 'start',
        elapsedTime > LOAD_TIME_MIN ? 0 : LOAD_TIME_MIN - elapsedTime
    );
}

setTimeout(() => {
    // gameView.resize();
    // theGame.start();
    // gameView.startLevel();
}, 100);

BOOT();

theGame.on('done',
    async function () {
        const data = await gameView.finish(this._stats);

        console.log(data);
    });

window.addEventListener('resize', () => gameView.resize());
