echo "const ALL_AUDIOS_NEGATIVE = [" > paths.js;

for path in negative/*; do
    echo "\"./sound/$path\"," >> paths.js
done

echo "]; \n" >> paths.js

echo "const ALL_AUDIOS_POSITIVE = [" >> paths.js
for path in positive/*; do
    echo "\"./sound/$path\"," >> paths.js
done

echo "];" >> paths.js